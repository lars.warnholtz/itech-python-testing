# -*- coding: utf-8 -*-
"""
In diesem Modul werden die Tests für das Modul apitime implementiert.
"""
import Uebungen_Integrationstests_mit_Mocking.apitime as apitime
import json
import unittest

import mock

class APITimeTestCase(unittest.TestCase):

    def setUp(self):
        """Diese Methode wird vor den eigentlichen Tests ausgeführt.
        In der Methode werden Testdaten erzeugt, die im weiteren für das Mocking verwendet werden.
        http://docs.python.org/library/unittest.html#class-and-module-fixtures
        """
        self.content = {
            "week_number": 44,
            "utc_offset": "+01:00",
            "utc_datetime": "2019-11-02T19:39:46.472442+00:00",
            "unixtime": 1572723586,
            "timezone": "Europe/Berlin",
            "raw_offset": 3600,
            "dst_until": "",
            "dst_offset": 0,
            "dst_from": "",
            "dst": False,
            "day_of_year": 306,
            "day_of_week": 6,
            "datetime": "2019-11-02T20:39:46.472442+01:00",
            "client_ip": "77.3.119.31",
            "abbreviation": "CET"}
        self.content_json = json.dumps(self.content)

    # Übung: Vervollständigen Sie die folgende Testmethode
    def test_get_time_ip(self):
        at = apitime.APITime()
        #Die Klasse APITime des Moduls apitime wird aufgerufen.
        with mock.patch.object(apitime.APITime, 'get_worldtimeapi_ip', return_value=self.content_json) as method:
            # Die Methode get_worldtimeapi_ip der Klasse APITime des Moduls apitime wird gepacht,
            # also mit einem Rückgabewert versehen, den Sie bestimmen können. Wie cool ist das?!?
            ip_datetime = at.get_datetime_ip()
            method.assert_called_once_with()
            self.assertEquals("","")
            # Hier müssen Sie aktiv werden, dieser Test ist momentan immer "grün"!

    # Übung: Vervollständigen Sie die folgende Testmethode
    def test_get_weeknumber_and_timezone(self):
        at = apitime.APITime()
        with mock.patch.object() as method:
            self.assertEquals("", "")


# Verbosity=2 sorgt dafür, dass die Ausgaben "wortreicher" sind.
# Wenn Sie den Level auf 1 ändern, erkennen Sie den Unterschied.
if __name__=='__main__':
    unittest.main(verbosity=2)
